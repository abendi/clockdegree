public class ClockDegreeCalc {

    public static void main(String[] args) {
        ClockDegreeCalc calc = new ClockDegreeCalc();
        calc.getAngle(Integer.valueOf(args[0]), Integer.valueOf(args[1]));
    }

    public int getAngle(int hour, int minute) {
        validateInput(hour, minute);

        hour = convertHourTo12Format(hour);
        if (hour == 12) {
            hour = 0;
        }
        if (minute == 60) {
            minute = 0;
        }
        int hourAngle = (int) Math.round(0.5 * (hour * 60 + minute));
        int minuteAngle = 6 * minute;

        int angle = Math.abs(hourAngle - minuteAngle);

        System.out.println(angle);
        return angle;
    }

    private int convertHourTo12Format(int hour) {
        if (hour > 12) {
            return hour - 12;
        }
        return hour;
    }

    private void validateInput(int hour, int minute) {
        if (hour < 0 || minute < 0 || hour > 24 || minute > 60) {
            throw new IllegalArgumentException();
        }
    }
}
