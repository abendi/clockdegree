import org.junit.Assert;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ClockDegreeCalcTest {
    private ClockDegreeCalc calc = new ClockDegreeCalc();

    @Test
    public void calcReturnsZeroAngle() {
        Assert.assertEquals(0, calc.getAngle(12, 0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void calcThrowsNegativeInput() {
        Assert.assertEquals(0, calc.getAngle(-5, 0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void calcThrowsInvalidInput() {
        Assert.assertEquals(0, calc.getAngle(5, 70));
    }

    @Test
    public void calcReturnsZeroAngle24HourFormat() {
        Assert.assertEquals(0, calc.getAngle(24, 0));
    }

    @Test
    public void calcReturnsCorrectAngleZeroMinutes() {
        Assert.assertEquals(270, calc.getAngle(9, 0));
    }

    @Test
    public void calcReturnsCorrectAngleWithMinutes() {
        Assert.assertEquals(15, calc.getAngle(6, 30));
    }

    @Test
    public void calcReturnsCorrectAngleWithMinutes24HourFormat() {
        Assert.assertEquals(15, calc.getAngle(18, 30));
    }



}
